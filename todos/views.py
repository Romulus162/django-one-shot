from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from .models import TodoList, TodoItem

from .form import CreateForm

# Create your views here.


def list(request):
    todos = TodoList.objects.all()
    context = {"todos": todos}
    return render(request, "todos.html", context)


def details(request, id):
    todo_details = get_object_or_404(TodoList, id=id)
    context = {"todo_details": todo_details}
    return render(request, "details.html", context)


def create(request):
    if request.method == "POST":
        form = CreateForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse("todo_list_list"))
    else:
        form = CreateForm()
    context = {"form": form}
    return render(request, "create.html", context)


def update(request, pk):
    todo_list = get_object_or_404(TodoList, pk=pk)
    form = CreateForm(request.POST or None, instance=todo_list)
    if request.method == "POST":
        if form.is_valid():
            todo_list = form.save()
            return redirect(reverse("todo_list_list"))
    else:
        context = {"form": form, "todo_list": todo_list}
        return render(request, "edit.html", context)
