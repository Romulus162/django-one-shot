# from django.shortcuts import render
# from todos.models import TodoList
# from django.forms import ModelForm


# class CreateForm(ModelForm):
#     class Meta:
#         model = TodoList
#         exclude = ["created_on"]

# def get(self, request):
#     return render(request, "create_todo_list.html")
from django import forms
from .models import TodoList
from django.views.generic.edit import DeleteView


class CreateForm(forms.ModelForm):
    class Meta:
        model = TodoList
        fields = ["name"]


class Delete(DeleteView):
    model = TodoList
    success_url = "/todos/"
    template_name = "delete.html"
