from django.urls import path
from .views import list, details, create, update
from .form import Delete

urlpatterns = [
    path("", list, name="todo_list_list"),
    # path("<int:id>/", details, name="details"),
    path("<int:id>/", details, name="todo_detail"),
    path("create/", create, name="todo_list_create"),
    path("<int:pk>/update/", update, name="update"),
    path("<int:pk>/delete/", Delete.as_view(), name="delete"),
]
